<?php

/**
 * Aperdia Test.
 */

namespace Aperdia\Test;

use Auth;
use Illuminate\Contracts\Console\Kernel;
use Illuminate\Foundation\Testing\TestCase as FrameworkTestCase;
use Hash;

/**
 * @group ignore
 */
abstract class TestCase extends FrameworkTestCase
{
    /**
     * The base URL to use while testing the application.
     *
     * @var string
     */
    protected $baseUrl;

    /**
     * Faker entity.
     *
     * @var Faker\Factory
     */
    protected $faker;

    /**
     * Application name for bootstrap.
     *
     * @var string
     */
    protected $application = 'undefined';

    /**
     * Setup the test environment.
     *
     * @return void
     */
    protected function setUp(): void
    {
        parent::setUp();

        $this->faker = \Faker\Factory::create();
    }

    /**
     * Creates the application.
     *
     * @return Symfony\Component\HttpKernel\HttpKernelInterface
     */
    public function createApplication()
    {
        $app = require __DIR__.'/../../../../'.$this->application.'/bootstrap/app.php';

        $app->make(Kernel::class)->bootstrap();

        Hash::setRounds(5);

        $this->baseUrl = config('app.url');

        return $app;
    }

    /**
     * Be connected.
     */
    protected function connected()
    {
        $user = Auth::loginUsingId(1);

        $this->be($user);
    }

    /**
     * Show console output.
     *
     * @param string $text
     */
    public function consoleLog($text)
    {
        fwrite(STDERR, $text."\n");
    }
}
