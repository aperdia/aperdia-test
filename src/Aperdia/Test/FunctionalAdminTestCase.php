<?php

/**
 * Aperdia Test.
 */

namespace Aperdia\Test;

/**
 * @group ignore
 */
abstract class FunctionalAdminTestCase extends TestCase
{
    /**
     * Entity element.
     *
     * @var \Aperdia\Common\Model\CommonEntity
     */
    protected $element;

    /**
     * Type.
     *
     * @var string
     */
    protected $type;

    /**
     * Test index.
     */
    public function testIndex()
    {
        $response = $this->get('/admin/'.$this->type);

        $response->assertStatus(200);
    }

    /**
     * Test create page.
     */
    public function testCreate()
    {
        $response = $this->get('/admin/'.$this->type.'/create');

        $response->assertStatus(200);
    }

    /**
     * Test edit page.
     */
    public function testEdit()
    {
        $response = $this->get('/admin/'.$this->type.'/'.($this->element->id + 99999).'/edit');

        $response->assertStatus(302);

        $response->assertRedirect('/admin/'.$this->type);

        $response = $this->get('/admin/'.$this->type.'/'.$this->element->id.'/edit');

        $response->assertStatus(200);
    }

    /**
     * Test delete page.
     */
    public function testGetDelete()
    {
        $response = $this->get('/admin/'.$this->type.'/'.($this->element->id + 999999).'/delete');

        $response->assertStatus(302);

        $response->assertRedirect('/admin/'.$this->type);

        $response = $this->get('/admin/'.$this->type.'/'.$this->element->id.'/delete');

        $response->assertStatus(200);
    }
}
