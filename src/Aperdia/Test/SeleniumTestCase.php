<?php

/**
 * Aperdia Test.
 */

namespace Aperdia\Test;

/**
 * @group ignore
 */
class SeleniumTestCase extends \PHPUnit_Extensions_Selenium2TestCase
{
    /**
     * setUp with basis config.
     *
     * @return void
     */
    protected function setUp(): void
    {
        $this->setHost(env('HOST', 'http://localhost'));
        $this->setPort(4444);
        $this->setBrowser('firefox');
    }

    /**
     * Tear down.
     *
     * @return void
     */
    protected function tearDown(): void
    {
        $this->stop();
    }
}
