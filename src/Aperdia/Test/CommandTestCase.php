<?php

/**
 * Aperdia Test.
 */

namespace Aperdia\Test;

/**
 * Test for commands.
 */
class CommandTestCase extends TestCase
{
    /**
     * Signature.
     *
     * @var string
     */
    protected $signature;

    /**
     * Run command and see exist.
     */
    public function testCommand()
    {
        $this->artisan($this->signature);

        $this->assertEquals(0, $this->code, 'Problem with command "{$this->signature}"');
    }
}
