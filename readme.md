# Aperdia Test

Quelques fonctions de base pour faire des tests.

## Functional admin test case

Pour tester les fonctions de base de l'administration ( index, create, edit, getDelete ).

    Aperdia\Test\FunctionalAdminTestCase

## Select test case

Pour les tests avec Selenium.

    Aperdia\Test\SeleniumTestCase

Pour choisir l'hôte, mettre à jour *phpunit.xml* avec *HOST*.

## Test case

Pour les tests de base avec l'initialisation de Faker, l'authentification, les logs.

    Aperdia\Test\TestCase
